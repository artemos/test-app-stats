<?php

namespace App\Services\Statistic;

use Illuminate\Support\Facades\Redis;

/**
 * Class RedisStatisticService
 * @package App\Services\Statistic
 */
class RedisStatisticService implements StatisticServiceInterface
{
    /**
     * Country codes
     * @var array
     */
    protected $codes;

    /**
     * RedisStatisticService constructor.
     * @param array $codes
     */
    public function __construct(array $codes)
    {
        $this->codes = $codes;
    }

    /**
     * @param $code
     * @return mixed|void
     */
    public function writeStat($code)
    {
        Redis::incr($code);
    }

    /**
     * @return array
     */
    public function readStat(): array
    {
        $count = Redis::mget($this->codes);
        $data  = [];

        foreach ($this->codes as $i => $code) {
            $data[$code] = (int) $count[$i];
        }

        return $data;
    }

    public function deleteStat()
    {
        foreach ($this->codes as $code) {
            Redis::del($code);
        }
    }
}