<?php

use App\Services\Statistic\RedisStatisticService;

/**
 * Class RedisStatisticServiceTest
 */
class RedisStatisticServiceTest extends TestCase
{
    /**
     * @var RedisStatisticService
     */
    protected static $service;

    /**
     * @var array 
     */
    protected static $counters = ['ru' => 10, 'us' => 5];


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        
        self::$service = new RedisStatisticService(array_keys(self::$counters));
        self::$service->deleteStat();
    }
    
    public function testWriteStat()
    {
        foreach (self::$counters as $code => $count) {
            for ($i = 0; $i < $count; $i++) {
                self::$service->writeStat($code);
            }
        }

        $this->assertTrue(true);
    }

    /**
     * @depends testWriteStat
     */
    public function testReadStat()
    {
        $data = self::$service->readStat();
        
        $this->assertIsArray($data);
        $this->assertNotEmpty($data);
        $this->assertEquals(self::$counters, $data);
    }
}