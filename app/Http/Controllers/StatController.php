<?php

namespace App\Http\Controllers;

use App\Services\Statistic\StatisticServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Services\Statistic\RedisStatisticService;

/**
 * Class StatController
 * @package App\Http\Controllers
 */
class StatController extends Controller
{
    /**
     * Allowed country codes
     * @var array
     */
    protected $codes;

    /**
     * @var StatisticServiceInterface
     */
    protected $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->codes   = $this->getCountryCodes();
        $this->service = new RedisStatisticService($this->codes);
    }

    /**
     * Display statistics
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = $this->service->readStat();
        return response()->json($data);
    }

    /**
     * Write statistics
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string|in:' . implode(',', $this->codes)
        ]);

        $this->service->writeStat($request->input('code'));

        return $this->response(['status' => 'success'], 201);
    }

    /**
     * Returns codes from config
     */
    protected function getCodesFromConfig()
    {
        $codes = getenv('COUNTRY_CODES');

        if (empty($codes)) {
            abort(500, "COUNTRY_CODES env variable is not set");
        }

        $codes = explode(",", $codes);
        
        foreach ($codes as $i => $code) {
            $codes[$i] = trim($code);
        }

        $codes = array_filter($codes);

        if (empty($codes)) {
            abort(500, "empty env variable COUNTRY_CODES");
        }

        return $codes;
    }

    /**
     * Returns codes from config
     * @return array
     */
    protected function getCountryCodes()
    {
        $codes = Redis::get('codes');

        if ($codes) {
            return explode(",", $codes);
        }
        
        $codes = $this->getCodesFromConfig();
        $expire = getenv('COUNTRY_CODES_EXPIRE', 60);

        Redis::set('codes', implode(',',$codes));
        Redis::expire('codes', $expire);
        
        return $codes;
    }
    
    /**
     * @param array $data
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response(array $data, $code)
    {
        return response()->json($data, $code, [], \JSON_UNESCAPED_UNICODE | \JSON_UNESCAPED_SLASHES);
    }
}
