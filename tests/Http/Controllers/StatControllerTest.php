<?php

/**
 * Class StatControllerTest
 */
class StatControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/api/stats')
            ->seeHeader('Content-Type', 'application/json')
            ->seeStatusCode(200);

        $content  = $this->response->getContent();
        $response = json_decode($content, true);

        if ($response === false) {
            $this->fail("Invalid JSON format {$content}");
        }

        $this->assertIsArray($response);
        $this->assertNotEmpty($response);

        $codes = $this->getCodes();

        foreach ($response as $code => $count) {
            $this->assertIsString($code);
            $this->assertIsInt($count);
            $this->assertContains($code, $codes);
        }
    }

    /**
     * @dataProvider codesProvider
     */
    public function testCreate($code)
    {
        $data = ['code' => $code];
        
        $this->post('/api/stats', $data)
            ->seeHeader('Content-Type', 'application/json')
            ->seeStatusCode(201)
            ->seeJson(['status' => 'success']);
    }

    public function testCreateWithoutCode()
    {
        $this->post('/api/stats')
            ->seeHeader('Content-Type', 'application/json')
            ->seeStatusCode(422)
            ->seeJson(['code' => ['The code field is required.']]);
    }

    public function testCreateWithWrongCode()
    {
        $this->post('/api/stats', ['code' => 'hz'])
            ->seeHeader('Content-Type', 'application/json')
            ->seeStatusCode(422)
            ->seeJson(['code' => ['The selected code is invalid.']]);
    }

    /**
     * @return array|false|string
     */
    public function codesProvider()
    {
        $data  = [];

        foreach ($this->getCodes() as $code) {
            $data[] = [$code];
        }
        
        return $data;
    }

    /**
     * @return array|false|string
     */
    protected function getCodes()
    {
        $codes = getenv('COUNTRY_CODES');
        $codes = explode(',', $codes);
        
        return $codes;
    }
}
