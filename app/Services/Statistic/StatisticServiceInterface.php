<?php

namespace App\Services\Statistic;

interface StatisticServiceInterface
{
    /**
     * StatisticServiceInterface constructor.
     * @param array $codes
     */
    public function __construct(array $codes);

    /**
     * Writes statistic
     * @param $code
     * @return mixed
     */
    public function writeStat($code);

    /**
     * Reads statistic
     * @return array
     */
    public function readStat() : array;
}